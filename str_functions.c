/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "str_functions.h"


inline u8 str_u8StrLength(const char* charPtrStrCpy)
{
	u8 i = 0;
	while (charPtrStrCpy[i++]); // count after null (length until null)

	return (i - 1); // don't count null
}

u8 str_u8StrCmp(const char* charPtrStr_1Cpy, const char* charPtrStr_2Cpy)
{
	if (str_u8StrLength(charPtrStr_1Cpy) != str_u8StrLength(charPtrStr_2Cpy))
		return 0;

	for (u8 i = 0; charPtrStr_1Cpy[i]; i++)
	{
		if (charPtrStr_1Cpy[i] != charPtrStr_2Cpy[i])
			return 0;
	}

	return 1;
}

void str_vidStrCopy(char* charPtrDestCpy, u8 u8LengthCpy, const char* charPtrSrcCpy)
{
	u8 i = 0;
	for (; (i < u8LengthCpy - 1) && charPtrSrcCpy[i]; i++)
		charPtrDestCpy[i] = charPtrSrcCpy[i];

	charPtrDestCpy[i] = 0; // properly null-terminate the dest str
}

